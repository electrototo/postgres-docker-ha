Note for future me: configure a repository to pull the image and
avoid copying the docker file and reacreate the image everytime
the file changes.

Issue:
It is not recommended to use build-time variables for passing secrets
like github keys, user credentials etc. Build-time variable values are
visible to any user of the image with the docker history command.

https://docs.docker.com/engine/reference/builder/#impact-on-build-caching
