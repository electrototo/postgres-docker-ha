#!/bin/sh

# Backup from the master db
rm -fr /var/lib/postgresql/9.5/main_old/
mv /var/lib/postgresql/9.5/main /var/lib/postgresql/9.5/main_old

su -c "echo 'repuser123' | pg_basebackup -h master -D /var/lib/postgresql/9.5/main -U repuser -v -P --xlog-method=stream" postgres

cp /usr/share/postgresql/9.5/recovery.conf /var/lib/postgresql/9.5/main/recovery.conf
cp /usr/share/postgresql/9.5/postgresql.conf /etc/postgresql/9.5/main/postgresql.conf

exit_status="$?"

if [ "$exit_status" = "0" ]; then
    # Execute the service as postgres
    su -c "/usr/lib/postgresql/9.5/bin/postgres -D /var/lib/postgresql/9.5/main -c config_file=/usr/share/postgresql/9.5/postgresql.conf" postgres
else:
    echo "Could not copy db"
    exit 1
fi
