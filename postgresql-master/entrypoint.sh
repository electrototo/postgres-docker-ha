#!/bin/sh

# Change mount permissions
chown postgres:postgres /var/lib/postgresql/9.5/main/
chmod 0700 /var/lib/postgresql/9.5/main/

# Execute the service as postgres
su -c "/usr/lib/postgresql/9.5/bin/postgres -D /var/lib/postgresql/9.5/main -c config_file=/etc/postgresql/9.5/main/postgresql.conf" postgres
